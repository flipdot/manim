#!/usr/bin/env python
from typing import Union

from big_ol_pile_of_manim_imports import *
import pathlib

# To watch one of these scenes, run the following:
# python -m manim example_scenes.py SquareToCircle -pl
#
# Use the flat -l for a faster rendering at a lower
# quality.
# Use -s to skip to the end and just save the final frame
# Use the -p to have the animation (or image, if -s was
# used) pop up once done.
# Use -n <number> to skip ahead to the n'th animation of a scene.

SVG = pathlib.Path(SVG_IMAGE_DIR)
PRIMARY_COLOR = '#f6c600'
SECONDARY_COLOR = '#000000'
FULL_LOGO_CIRCLE_DISTANCE = 3.0
LETTER_ORIGIN = {
    'f': ORIGIN + LEFT * .8,
    'd': ORIGIN + RIGHT * .5,
    'lip': ORIGIN + UP + LEFT * 3.8,
    'ot': ORIGIN + UP + RIGHT * 4,
    'flip': ORIGIN + UP / 1.3 + LEFT * FULL_LOGO_CIRCLE_DISTANCE,
    'dot': ORIGIN + UP + RIGHT * FULL_LOGO_CIRCLE_DISTANCE,
    'hackerspace_kassel': ORIGIN + DOWN * 3,
}

DEBUG = False


class SingleLogo(Scene):
    CONFIG = {
        'circle_animation': Write,
        'circle_animation_kwargs': {},
        'camera_config': {
            'background_color': Color('#000000')
        },
    }

    def construct(self):
        circle, letters = self.show_fd_circle()
        self.wait(3)
        self.fade_out(circle, letters)

    def show_fd_circle(self, origin=ORIGIN):
        circle = self.load_circle('circle', origin=origin)
        circle_dent = self.load_circle('circle_dent', origin=origin)
        letter_f = self.load_letter('f', origin=origin)
        letter_d = self.load_letter('d', origin=origin)

        # group both letters together, so the "Write()" animation works smoothly
        letters = VMobject()
        letters.add(letter_f)
        letters.add(letter_d)

        self.play(self.circle_animation(circle, stroke_width=15))
        self.play(
            Transform(circle, circle_dent, run_time=1.5),
            Write(letters, run_time=2),
        )
        return circle, letters

    def load_circle(self, filename, origin=ORIGIN) -> SVGMobject:
        obj = SVGMobject(file_name=str(SVG / f'{filename}.svg'))
        obj.set_width(5.6)
        obj.set_stroke(PRIMARY_COLOR, width=15)
        obj.set_fill(PRIMARY_COLOR)
        obj.move_to(origin)
        return obj

    def load_letter(self, letter, origin=ORIGIN) -> SVGMobject:
        obj = SVGMobject(file_name=str(SVG / f'letter_{letter}.svg'))
        height = 2.2
        if letter == 'flip':
            height *= 1.3
        obj.set_height(height)
        obj.move_to(LETTER_ORIGIN[letter] + origin)
        obj.set_stroke(SECONDARY_COLOR)
        obj.set_fill(SECONDARY_COLOR)
        return obj

    def fade_out(self, *args):
        self.play(*[FadeToColor(x, self.camera_config['background_color']) for x in args if x is not None])


class SingleLogoInverted(SingleLogo):
    CONFIG = {
        'circle_animation': ShowCreation,
    }

    def load_circle(self, filename, *args, **kwargs) -> SVGMobject:
        obj = super().load_circle(filename, *args, **kwargs)
        obj.set_stroke(PRIMARY_COLOR, width=15)
        obj.set_fill(SECONDARY_COLOR)
        return obj

    def load_letter(self, letter, *args, **kwargs) -> SVGMobject:
        obj = super().load_letter(letter, *args, **kwargs)
        obj.set_stroke(PRIMARY_COLOR, width=1)
        obj.set_fill(PRIMARY_COLOR)
        return obj


class SingleLogoWhiteBG(SingleLogo):
    CONFIG = {
        'camera_config': {
            'background_color': Color('#ffffff')
        },
    }


class SingleLogoInvertedWhiteBG(SingleLogoInverted):
    CONFIG = {
        'camera_config': {
            'background_color': Color('#ffffff')
        },
        'circle_animation': DrawBorderThenFill
    }


class FullLogo(SingleLogo):

    def construct(self):
        black_circle = Circle()
        black_circle.set_stroke(opacity=0)
        self.add(black_circle)

        yellow_circle, letters = self.show_fd_circle()
        letter_f, letter_d = letters.submobjects

        left_objects = VMobject()
        left_objects.add(letter_f)
        left_objects.add(yellow_circle)

        black_circle_ = self.load_black_circle()
        self.play(Transform(black_circle, black_circle_, run_time=0))

        line_to_left = Line(start=ORIGIN, end=UP + LEFT * FULL_LOGO_CIRCLE_DISTANCE, color='#ff00ff')
        line_to_right = Line(start=ORIGIN, end=UP + RIGHT * FULL_LOGO_CIRCLE_DISTANCE, color='#ff00ff')

        letter_flip = self.load_letter('flip')
        letter_dot = self.load_letter('dot')
        letter_dot.set_fill(PRIMARY_COLOR)
        letter_hackerspace_kassel = self.load_hackerspace()

        self.play(
            MoveAlongPath(yellow_circle, line_to_left, run_time=1.5),
            MoveAlongPath(black_circle, line_to_right, run_time=1.5),
            Transform(letter_f, letter_flip, run_time=1.5),
            Transform(letter_d, letter_dot, run_time=1.5),
            DrawBorderThenFill(letter_hackerspace_kassel, run_time=1.8),
        )
        self.wait(2)
        self.fade_out(
            yellow_circle,
            black_circle,
            letter_f,
            letter_d,
            letter_hackerspace_kassel,
        )

    def load_black_circle(self):
        circle = self.load_circle('circle_dent')
        circle.set_stroke(width=15)
        circle.set_fill(SECONDARY_COLOR)
        return circle

    def load_hackerspace(self):
        font = self.load_letter('hackerspace_kassel')
        font.set_stroke(PRIMARY_COLOR, width=1)
        font.set_fill(PRIMARY_COLOR)
        font.set_height(1.3)
        return font


class FullLogoWhiteBG(FullLogo):
    CONFIG = {
        'camera_config': {
            'background_color': Color('#ffffff')
        },
    }

    def load_black_circle(self):
        circle = super().load_black_circle()
        circle.set_stroke(SECONDARY_COLOR, width=1)
        return circle

    def load_hackerspace(self):
        font = super().load_hackerspace()
        font.set_stroke(SECONDARY_COLOR)
        font.set_fill(SECONDARY_COLOR)
        return font
