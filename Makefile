SHELL := /bin/bash

ifeq ($(origin SCENE), undefined)
	FILENAME := *.mp4
else
	FILENAME := $(SCENE).mp4
endif

SCENE ?= -a
RES ?= 1080p

all: 1080p 4k

clean:
	rm -rf media/videos/projects/flipdot

play:
	@if [ "$(RES)" = "4k" ]; then \
	mpv --loop-playlist --fullscreen  media/videos/projects/flipdot/2160p60/$(FILENAME); \
	else \
	mpv --loop-playlist --fullscreen  media/videos/projects/flipdot/$(RES)60/$(FILENAME); \
	fi;


lowres:
	python manim.py -l projects/flipdot.py $(SCENE)
	mpv --loop-playlist media/videos/projects/flipdot/480p15/$(FILENAME)

720p:
	python manim.py -r 720,1280 projects/flipdot.py $(SCENE)

1080p:
	python manim.py -r 1080,1920 projects/flipdot.py $(SCENE)

4k:
	python manim.py -r 2160,3840 projects/flipdot.py $(SCENE)